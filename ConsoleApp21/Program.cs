﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp21
{
    class Program
    {
        static void Main(string[] args)
        {
            new Teste().Vai();
        }
    }

    public class Teste
    {
        public void Vai()
        {
            var ret = GetMethodName(() => Galo("icaro"));

            var ret2 = GetMethodName(() => Galo("icaro", 31));
        }

        public T GetMethodName<T>(Func<T> method) where T : class
        {
            // chama o metodo, neste caso não é importante rsr
            var result = method();

            // preciso que a variavel methodName contenha o nome do método da seguinte forma
            // assemblyName + namespace + class name + method name + parameters, 
            // EX.
            // ConsoleApp21.ConsoleApp21.Teste.Galo.string
            // ConsoleApp21.ConsoleApp21.Teste.Galo.string.int

            var methodName = string.Empty;

            return methodName as T;
        }

        public string Galo(string nome)
        {
            return nome + " galo13";
        }

        public string Galo(string nome, int idade)
        {
            return nome + idade + " galo13";
        }
    }
}